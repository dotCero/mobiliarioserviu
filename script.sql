create database mobiliarioServiu;
use mobiliarioServiu;

-- drop database mobiliarioServiu;

create table funcionario(
    id int auto_increment primary key,
    rut varchar(10),
    nombre varchar(255),
    depto varchar(255),
    unidad varchar(255),
    cargo varchar(255),
    iniciales varchar(3)
);

create table mobiliario(
    id int auto_increment primary key,
    codigo varchar(255),
    nombre varchar(255),
    observacion varchar(255),
    valor bigint,
    fechaAdquisicion date,
    vidaUtil date,
    depreciacion bigint,
    valorResidual bigint,
    valorActualizado bigint
);

create table asignacion(
    id int auto_increment primary key,
    idFuncionario int,
    idMobiliario int,
    cantidadMobiliario int,
    codigoAsignacion int,
    codigoBarras varchar(255),
    foreign key (idFuncionario) references funcionario(id) ON DELETE CASCADE,
    foreign key (idMobiliario) references mobiliario(id) ON DELETE CASCADE
);

create table populate(
    id int AUTO_INCREMENT PRIMARY KEY,
    path VARCHAR(511)
);

create table populateII(
    id int AUTO_INCREMENT PRIMARY KEY,
    path VARCHAR(511)
);

-- select * from funcionario;
-- select * from mobiliario;
-- select * from asignacion;

-- select mobiliario.codigo, mobiliario.nombre, count(1) as cuenta from asignacion inner join mobiliario on asignacion.idMobiliario = mobiliario.id group by asignacion.idMobiliario

-- LOAD DATA LOCAL INFILE "/home/cero/Escritorio/SELECT_t___FROM_mobiliarioServiu_funcion.csv" INTO TABLE funcionario

-- CHARACTER SET UTF8
-- FIELDS TERMINATED BY ","
-- LINES TERMINATED BY "\n"
-- (funcionario.rut, funcionario.nombre, funcionario.depto, funcionario.unidad, funcionario.cargo, funcionario.iniciales, funcionario.apellidoNombre)
-- set id = @@identity + 1;