/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  cero
 * Created: 16-05-2017
 */

use mobiliarioServiu;

alter table mobiliario add column grupo int;
alter table mobiliario add column categoria varchar(50);
alter table mobiliario add column subgrupo int;
alter table mobiliario add column medidas varchar(100);
alter table mobiliario add column caracteristicas varchar(200);


alter table funcionario add column apellidoNombre varchar(300);




INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('111','Escritorio en "L"','Escritorio en "L" 1,80x1,70x0,7x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '11','1,80x1,70x0,7x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('112','Escritorio en "L"','Escritorio en "L" 1,60x1,60x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '12','1,60x1,60x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('113','Escritorio en "L" con Extensión','Escritorio en "L" con Extensión 1,60x2,20x0,6x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '13','1,60x2,20x0,6x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('114','Escritorio en "L"','Escritorio en "L" 1,60x1,70x0,6x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '14','1,60x1,70x0,6x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('115','Escritorio en "L"','Escritorio en "L" 1,50x2,00x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '15','1,50x2,00x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('116','Escritorio en "L"','Escritorio en "L" 1,92x1,65x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '16','1,92x1,65x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('117','Escritorio en "L"','Escritorio en "L" 1,80x1,50x0,7x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '17','1,80x1,50x0,7x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('118','Escritorio en "L"','Escritorio en "L" 2,73x1,25x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '18','2,73x1,25x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('119','Escritorio en "L"','Escritorio en "L" 1,60x1,10x00,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '19','1,60x1,10x00,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('120','Escritorio en "L"','Escritorio en "L" 2,45x1,10x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '20','2,45x1,10x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('121','Escritorio en "L"','Escritorio en "L" 1,70x1,10x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '21','1,70x1,10x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('122','Escritorio en "L"','Escritorio en "L" 1,70x1,70x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '22','1,70x1,70x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('123','Escritorio en "L"','Escritorio en "L" 2,45x1,53x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '23','2,45x1,53x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('124','Escritorio en "L"','Escritorio en "L" 1,53x1,60x0,7x0,70x,075 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '24','1,53x1,60x0,7x0,70x,075','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('125','Escritorio en "L"','Escritorio en "L" 1,80x1,60x0,7x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '25','1,80x1,60x0,7x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('126','Escritorio en "L"','Escritorio en "L" 1,87x1,56x0,7x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '26','1,87x1,56x0,7x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('127','Escritorio Rectangular','Escritorio Rectangular 1,62x0,7x075 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '27','1,62x0,7x075','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('128','Escritorio Rectangular','Escritorio Rectangular 1,50x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '28','1,50x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('129','Escritorio Rectangular','Escritorio Rectangular 1,80x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '29','1,80x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('130','Escritorio Rectangular','Escritorio Rectangular 1,46x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '30','1,46x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('131','Escritorio Rectangular','Escritorio Rectangular 2,50x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '31','2,50x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('132','Escritorio Rectangular','Escritorio Rectangular 1,72x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '32','1,72x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('133','Escritorio Rectangular','Escritorio Rectangular 2,33x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '33','2,33x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('134','Escritorio Rectangular','Escritorio Rectangular 1,85x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '34','1,85x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('135','Escritorio Rectangular','Escritorio Rectangular 1,23x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '35','1,23x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('136','Escritorio Rectangular','Escritorio Rectangular 1,25x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '36','1,25x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('137','Escritorio con Estante','Escritorio con Estante 4,06x1,10x0,64x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '37','4,06x1,10x0,64x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('138','Escritorio Rectangular pta. Roma','Escritorio Rectangular pta. Roma 1,60x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '38','1,60x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('139','Extensión Escritorio','Extensión Escritorio 1,00x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','1','Escritorio', '39','1,00x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('211','Base','Base 1,95x035x0,80 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '11','1,95x035x0,80','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('212','Base','Base 0,9x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '12','0,9x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('213','Base','Base 1,37x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '13','1,37x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('214','Base','Base 2,34x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '14','2,34x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('215','Base','Base 1,80x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '15','1,80x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('216','Base','Base 0,95x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '16','0,95x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('217','Base','Base 0,48x0,45x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '17','0,48x0,45x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('218','Base','Base 1,15x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '18','1,15x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('219','Base','Base 1,00x0,67x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '19','1,00x0,67x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('220','Base','Base 1,50x0,3x1,22 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '20','1,50x0,3x1,22','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('221','Base','Base 0,54x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '21','0,54x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('222','Base','Base 1,00x0,5x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '22','1,00x0,5x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('223','Base','Base 1,60x0,53x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '23','1,60x0,53x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('224','Base','Base 0,8x0,53x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '24','0,8x0,53x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('225','Base','Base 1,00x0,45x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '25','1,00x0,45x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('226','Base','Base 1,20x0,42x0,70 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '26','1,20x0,42x0,70','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('227','Base','Base 2,00x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '27','2,00x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('228','Base','Base 2,14x0,45x0,9 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '28','2,14x0,45x0,9','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('229','Base','Base 0,9x0,5x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '29','0,9x0,5x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('230','Base','Base 1,04x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '30','1,04x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('231','Base','Base 1,36x0,5x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '31','1,36x0,5x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('232','Base','Base 1,62x0,45x0,9 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '32','1,62x0,45x0,9','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('233','Base','Base 1,18x0,5x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '33','1,18x0,5x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('234','Base','Base 1,80x0,5x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '34','1,80x0,5x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('235','Base','Base 0,49x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '35','0,49x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('236','Base','Base 0,49x0,46x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '36','0,49x0,46x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('237','Base','Base 0,9x0,46x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '37','0,9x0,46x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('238','Base','Base 0,9x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '38','0,9x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('239','Base','Base 1,33x0,6x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','2','Muebles 
Base', '39','1,33x0,6x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('311','Biblioteca','Biblioteca 0,8x0,35x1,95 Color Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '11','0,8x0,35x1,95','Color Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('312','Biblioteca','Biblioteca 0,75x0,35x1,95 Color Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '12','0,75x0,35x1,95','Color Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('313','Biblioteca 2 Cuerpos','Biblioteca 2 Cuerpos 1,40x0,32x1,92 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '13','1,40x0,32x1,92','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('314','Biblioteca','Biblioteca 1,12x0,3x1,22 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '14','1,12x0,3x1,22','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('315','Biblioteca','Biblioteca 1,20x0,3x1,22 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '15','1,20x0,3x1,22','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('316','Biblioteca','Biblioteca 1,80x0,3x1,22 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '16','1,80x0,3x1,22','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('317','Biblioteca','Biblioteca 1,60x0,35x1,95 Color Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '17','1,60x0,35x1,95','Color Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('318','Biblioteca','Biblioteca 0,9x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '18','0,9x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('319','Biblioteca','Biblioteca 0,45x0,3x1,63 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '19','0,45x0,3x1,63','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('320','Biblioteca','Biblioteca 1,93x0,37x2,44 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '20','1,93x0,37x2,44','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('321','Biblioteca','Biblioteca 1,60x0,35x0,7 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '21','1,60x0,35x0,7','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('322','Biblioteca','Biblioteca 0,7x0,4x1,92 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '22','0,7x0,4x1,92','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('323','Biblioteca','Biblioteca 0,9x0,3x1,20 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '23','0,9x0,3x1,20','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('324','Biblioteca','Biblioteca 0,9x0,35x2,05 Color Cerezo',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '24','0,9x0,35x2,05','Color Cerezo');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('325','Biblioteca','Biblioteca 1,40x0,48x1,90 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '25','1,40x0,48x1,90','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('326','Biblioteca','Biblioteca 0,9x0,45x2,15 Color Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '26','0,9x0,45x2,15','Color Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('327','Biblioteca Dual','Biblioteca Dual 1,80x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '27','1,80x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('328','Biblioteca Dual','Biblioteca Dual 1,80x0,36x0,75 Color Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '28','1,80x0,36x0,75','Color Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('329','Biblioteca','Biblioteca 0,46x0,49x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '29','0,46x0,49x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('330','Biblioteca Duplex','Biblioteca Duplex 1,40x0,5x0,7 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '30','1,40x0,5x0,7','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('331','Biblioteca Duplex','Biblioteca Duplex 1,40x0,32x1,20 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '31','1,40x0,32x1,20','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('332','Biblioteca','Biblioteca 1,5x0,36x1,95 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '32','1,5x0,36x1,95','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('333','Biblioteca','Biblioteca 1,60x0,7x0,7 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '33','1,60x0,7x0,7','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('334','Biblioteca','Biblioteca 1,60x0,33x1,38 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '34','1,60x0,33x1,38','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('335','Biblioteca','Biblioteca 2,20x0,3x1,84 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','3','Biblioteca', '35','2,20x0,3x1,84','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('411','Estantería Mural','Estantería Mural 6,17x0,3x2,50 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','4','Estantería', '11','6,17x0,3x2,50','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('412','Estante Escritorio','Estante Escritorio 0,7x0,25x0,8 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','4','Estantería', '12','0,7x0,25x0,8','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('413','Armario','Armario 0,8x0,39x2,05 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','4','Estantería', '13','0,8x0,39x2,05','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('414','Estantería Mural','Estantería Mural 0,9x0,43x2,45 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','4','Estantería', '14','0,9x0,43x2,45','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('511','Cajonera','Cajonera 0,57x0,5x0,7 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','5','Cajoneras', '11','0,57x0,5x0,7','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('512','Cajonera','Cajonera 0,6x0,45x0,75 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','5','Cajoneras', '12','0,6x0,45x0,75','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('513','Cajonera','Cajonera 0,5x0,4x0,73 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','5','Cajoneras', '13','0,5x0,4x0,73','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('514','Cajonera','Cajonera 0,45x0,46x0,5 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','5','Cajoneras', '14','0,45x0,46x0,5','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('515','Cajonera','Cajonera 0,46x0,60x0,75 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','5','Cajoneras', '15','0,46x0,60x0,75','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('516','Cajonera','Cajonera 0,6x0,7x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','5','Cajoneras', '16','0,6x0,7x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('611','Kardex','Kardex 0,6x0,5x1,38 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','6','Kardex', '11','0,6x0,5x1,38','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('612','kardex','kardex 0,6x0,48x1,18 Color Café Claro',' 0', date(now()), date(now()), ' 0', '0 ','0','6','Kardex', '12','0,6x0,48x1,18','Color Café Claro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('711','Mesa Redonda','Mesa Redonda 0,9x0,75 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','7','Mesas', '11','0,9x0,75','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('712','Mesa Esquinera Baja','Mesa Esquinera Baja 0,5x0,5x0,4 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','7','Mesas', '12','0,5x0,5x0,4','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('713','Mesa Redonda','Mesa Redonda 1,17x0,75 Color Cerezo',' 0', date(now()), date(now()), ' 0', '0 ','0','7','Mesas', '13','1,17x0,75','Color Cerezo');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('714','Mesa Redonda','Mesa Redonda 1,20x0,45 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','7','Mesas', '14','1,20x0,45','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('715','Mesa de Centro Rectangular','Mesa de Centro Rectangular 1,20x0,7x0,4 Color Peral/Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','7','Mesas', '15','1,20x0,7x0,4','Color Peral/Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('811','Silla Escritorio Ergonométrica de Cuero','Silla Escritorio Ergonométrica de Cuero  Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '11','','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('812','Silla Escritorio Ergonométrica','Silla Escritorio Ergonométrica  Color Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '12','','Color Gris');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('813','Silla Atención de Público con apoyabrazos','Silla Atención de Público con apoyabrazos  Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '13','','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('814','Silla Atención de Público con apoyabrazos','Silla Atención de Público con apoyabrazos  Color Azul',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '14','','Color Azul');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('815','Silla Escritorio Ergonométrica','Silla Escritorio Ergonométrica  Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '15','','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('816','Silla Plegable','Silla Plegable  Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '16','','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('817','Silla Atención de Público sin apoyabrazos','Silla Atención de Público sin apoyabrazos  Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '17','','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('818','Silla Atención de Público sin apoyabrazos','Silla Atención de Público sin apoyabrazos  Color Azul',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '18','','Color Azul');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('819','Silla de espera triple','Silla de espera triple  Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '19','','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('820','Silla Escritorio Ejecutiva','Silla Escritorio Ejecutiva  Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '20','','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('821','Silla Atención de Público sin apoyabrazos','Silla Atención de Público sin apoyabrazos  Color Rojo',' 0', date(now()), date(now()), ' 0', '0 ','0','9','Sillones', '21','','Color Rojo');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('911','Sillon Cuerina Individual','Sillon Cuerina Individual 0,9x0,9x0,7 Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','8','Sillas', '11','0,9x0,9x0,7','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('912','Sofa Cuerina 3 cuerpos','Sofa Cuerina 3 cuerpos 1,50x0,75x0,65 Color Negro',' 0', date(now()), date(now()), ' 0', '0 ','0','9','Sillones', '12','1,50x0,75x0,65','Color Negro');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('1011','Caja Fuerte','Caja Fuerte 0,44x0,4x0,3 Acero keypad digital',' 0', date(now()), date(now()), ' 0', '0 ','0','10','Cajas 
Fuertes', '11','0,44x0,4x0,3','Acero keypad digital');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('1012','Caja Fuerte','Caja Fuerte 0,42x0,47x0,76 Acero keypad digital',' 0', date(now()), date(now()), ' 0', '0 ','0','10','Cajas 
Fuertes', '12','0,42x0,47x0,76','Acero keypad digital');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('1013','Caja Fuerte','Caja Fuerte 0,4x0,46x0,3 Acero keypad digital',' 0', date(now()), date(now()), ' 0', '0 ','0','10','Cajas 
Fuertes', '13','0,4x0,46x0,3','Acero keypad digital');

INSERT INTO `mobiliarioServiu`.`mobiliario`
(
`codigo`,
`nombre`,
`observacion`,
`valor`,
`fechaAdquisicion`,
`vidaUtil`,
`depreciacion`,
`valorResidual`,
`valorActualizado`, 
`grupo`, 
`categoria`,
`subgrupo`,
`medidas`,
`caracteristicas`)
VALUES
('1111','Porta CPU','Porta CPU 0,45x0,3x0,55 Color Gris',' 0', date(now()), date(now()), ' 0', '0 ','0','11','Otros', '11','0,45x0,3x0,55','Color Gris');

/*
alter table funcionario modify iniciales varchar(5) ;
  */
  

INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('7553391-8','Ema Abarca Duran','Operaciones Habitacionales','Pagos de Subsidios','','EAD','Abarca Duran, Ema');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16321958-1','Cesar Acevedo Alarcon','Técnico','Obras Civiles','','CAA','Acevedo Alarcon, Cesar');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15123094-6','Karen Acevedo Padilla','Técnico','Campamentos y regeneración de condominios sociales','','KAP','Acevedo Padilla, Karen Johana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13956340-9','Pamela Aguilera Zambrano','Técnico','Evaluación de proyectos y programa de mejoramiento de vivindas y barrios','','PAZ','Aguilera Zambrano, Pamela Raquel');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16166084-1','Cristian Allende Becerra','Técnico','Arrastre y coordinación de EGIS SERVIU','','CAB','Allende Becerra, Cristian Mauricio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15539438-2','Rodrigo Alvarez Alfaro','Técnico','Estudios y proyectos','','RAA','Alvarez Alfaro, Rodrigo Gonzalo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10187184-3','Sandra Alvarez Ibarra','Administración y Finanzas','Oficina de partes','','SAI','Alvarez Ibarra, Sandra Fabiola');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13721540-3','Tatiana Apablaza Soto','Técnico','Estudios y proyectos','','TAS','Apablaza Soto, Tatiana Marcela');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15527044-6','Carlos Arancibia Coñoeman','Operaciones Habitacionales','Social','Externo','CAC','Arancibia Coñoeman, Carlos');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16254954-5','Carlos Arancibia González','Técnico','Evaluación de proyectos y programa de mejoramiento de vivindas y barrios','','CAG','Arancibia González, Carlos Luis');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10874172-4','Ivonne Aravena Miranda','Técnico','Obras Civiles','','IAM','Aravena Miranda, Ivonne');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10582540-4','Alejandra Aravena Muñoz','Delegación Santa Cruz','-','','AAM','Aravena Muñoz, Alejandra Marcela');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15992632-K','Angelica Arellano Dominguez','Técnico','Campamentos y regeneración de condominios sociales','','AAD','Arellano Dominguez, Angelica');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13302220-1','Nora Arriagada Pavez','Técnico','Campamentos y regeneración de condominios sociales','','NAP','Arriagada Pavez, Nora');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17520199-8','Daniela Arriola Reinoso','Administración y Finanzas','Recursos Humanos','','DAR','Arriola Reinoso, Daniela Paz');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13097336-1','Miriam Astorga Barra','Técnico','Obras habitacionales y asistencia técnica','','MAB','Astorga Barra, Miriam Irene');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12938572-3','Pablo Avalos Herrera','Técnico','Obras habitacionales y asistencia técnica','','PAH','Avalos Herrera, Pablo Cesar');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14013016-8','Francisco Avila Figueroa','Técnico','Estudios y proyectos','','FAF','Avila Figueroa, Francisco Javier');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13962577-3','Felipe Bahamonde Migueles','Delegación Provincial Colchagua','-','','FBM','Bahamonde Migueles, Felipe Andres');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13125629-9','Ana Banda Cabrera','Operaciones Habitacionales','Postulaciones','','ABC','Banda Cabrera, Ana Maria');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17505035-3','Paula Banda Cabrera','Operaciones Habitacionales','Pagos de Subsidios','','PBC','Banda Cabrera, Paula Belen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('5309091-5','Luis Banda Campos','Programación Física y Control','-','','LBC','Banda Campos, Luis Custodio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9229779-9','Jaime Barahona Velasco','Jurídico','-','','JBV','Barahona Velasco, Jaime Alexis');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13781454-4','Ruth Barraza Lizana','Jurídico','-','','RBL','Barraza Lizana, Ruth Silvana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8282172-4','Glenda Barrios Salinas','Técnico','Obras habitacionales y asistencia técnica','','GBS','Barrios Salinas, Glenda Elizabeth');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16882268-5','Carolina Becerra Cespedes','Administración y Finanzas','Recursos Humanos','','CBC','Becerra Cespedes, Carolina Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('5278657-6','Selma Benavides Vargas','Delegación Provincial Colchagua','-','','SBV','Benavides Vargas, Selma Del');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10984102-1','Dante Bossi Molina','Administración y Finanzas','-','','DBM','Bossi Molina, Dante Giovanni');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15104271-6','Juan Briceño Ansoleaga','Jurídico','-','','JBA','Briceño Ansoleaga, Juan Ignacio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('7526525-5','Cesar Bruning Maldonado','Técnico','Estudios y proyectos','','CBM','Bruning Maldonado, Cesar');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17028395-3','Carlos Burgos Sepulveda','Técnico','Obras habitacionales y asistencia técnica','','CBS','Burgos Sepulveda, Carlos Patricio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17061126-8','Jaime Bustos Mendez','Dirección','Participación Ciudadana','','JBM','Bustos Mendez, Jaime Andres');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15174041-3','Carlos Cabrera Espinoza','Programación Física y Control','-','','CCE','Cabrera Espinoza, Carlos Andres');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15123021-0','Paulina  Cadiz Monsalve','Operaciones Habitacionales','Social','','PCM','Cadiz Monsalve, Paulina ');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11757169-6','Dolores Campos Chavez','Técnico','Obras habitacionales y asistencia técnica','Externo','DCC','Campos Chavez, Dolores Del Carmen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13011297-8','Fanny Cañas Rojas','Programación Física y Control','Gestión de información territorial y suelos','','FCR','Cañas Rojas, Fanny Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10005343-8','Oriana Cardenas Pinto','Programación Física y Control','Gestión de información territorial y suelos','','OCP','Cardenas Pinto, Oriana Ximena');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8209168-8','Victor Cardenas Valenzuela','Dirección','-','Director','VCV','Cardenas Valenzuela, Victor Javier');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13781791-8','Paz Carrasco Mella','Técnico','Obras Civiles','','PCM','Carrasco Mella, Paz Digla');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8668609-0','Carlos Carrasco Olea','Dirección','Comunicaciones y relaciones públicas','','CCO','Carrasco Olea, Carlos Javier');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13718240-8','Dennisse Carrasco Rojas','Dirección','O.I.R.S.','','DCR','Carrasco Rojas, Dennisse');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15865822-4','Francisca Carrasco Trigo','Administración y Finanzas','Bienestar','','FCT','Carrasco Trigo, Francisca');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11889912-1','Miriam Carvajal Medel','Técnico','Obras habitacionales y asistencia técnica','','MCM','Carvajal Medel, Miriam Del Carmen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('7108422-1','Marcial Casanova Salinas','Administración y Finanzas','Oficina de partes','','MCS','Casanova Salinas, Marcial');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11143977-K','Gladys Catalan Parraguez','Unidad  Serv. Generales','','','GCP','Catalan Parraguez, Gladys');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15125538-8','Esteban Cespedes Espinoza','Administración y Finanzas','Contabilidad y presupuesto','','ECE','Cespedes Espinoza, Esteban Gustavo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('7707459-7','Daniel Chamorro Letelier','Técnico','Obras habitacionales y asistencia técnica','','DCL','Chamorro Letelier, Daniel');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15991373-2','Andres Chirino Fernandez','Programación Física y Control','Gestión de información territorial y suelos','','ACF','Chirino Fernandez, Andres Arturo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6940058-2','Luis Constanzo Roa','Delegación Santa Cruz','-','','LCR','Constanzo Roa, Luis Alberto');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15117852-9','Goselin Cornejo Diaz','Técnico','Evaluación de proyectos y programa de mejoramiento de vivindas y barrios','','GCD','Cornejo Diaz, Goselin Julia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13776524-1','Vanesa Cornejo Galarce','Técnico','Ingeniería y vialidad urbana','','VCG','Cornejo Galarce, Vanesa');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('18801134-9','Melanie Correa Escobedo','Jurídico','-','Externo','MCE','Correa Escobedo, Melanie');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17414295-5','Denisse Correa González','Dirección','O.I.R.S.','','DCG','Correa González, Denisse Del Carmen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15125442-K','Valeria Correa Villaseca','Programación Física y Control','-','','VCV','Correa Villaseca, Valeria Del Pilar');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16521854-K','Paula Cortez Aguirre','Técnico','Estudios y proyectos','','PCA','Cortez Aguirre, Paula Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10661315-K','Claudio Cumsille Chomali','Delegación Santa Cruz','-','','CCC','Cumsille Chomali, Claudio Abrahma');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13456217-K','Claudio Diaz Merino','Dirección','Contraloria Interna','','CDM','Diaz Merino, Claudio Antonio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15492326-8','Pablo Diaz Moreno','Oficina Pichilemu','-','','PDM','Diaz Moreno, Pablo Andres');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15802757-7','Nadia Donoso Arce','Delegación Santa Cruz','-','','NDA','Donoso Arce, Nadia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14306799-8','Rodrigo Donoso Leon','Delegación Santa Cruz','-','','RDL','Donoso Leon, Rodrigo Antonio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15525028-3','Maria Teresa Espeleta Matu','Técnico','Evaluación de proyectos y programa de mejoramiento de vivindas y barrios','','MEM','Espeleta Matus, Maria Teresa De Jesus ');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13478509-8','Karin Espinoza Acevedo','Técnico','Arrastre y coordinación de EGIS SERVIU','Externo','KEA','Espinoza Acevedo, Karin');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12966885-7','Patricio Espinoza Tapia','Delegación Santa Cruz','','Externo','PET','Espinoza Tapia, Patricio Alejandro');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15994507-3','Susana Fagalde Gutierrez','Técnico','Campamentos y regeneración de condominios sociales','','SFG','Fagalde Gutierrez, Susana Daniela');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14519590-K','Paula Farias Arenas','Delegación Santa Cruz','-','','PFA','Farias Arenas, Paula Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('18616814-3','Marjorie Farias Montecinos','Delegación Santa Cruz','-','','MFM','Farias Montecinos, Marjorie Ruth');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11951221-2','Johana Farias Moore','Administración y Finanzas','Recursos Humanos','','JFM','Farias Moore, Johana Danina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12778571-6','Geovanni Fariña De La Hoz','Técnico','Arrastre y coordinación de EGIS SERVIU','','GFH','Fariña De La Hoz, Geovanni');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('5013882-8','Manuel Faundez Rivera','Técnico','Ingeniería y vialidad urbana','','MFR','Faundez Rivera, Manuel Patricio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16702381-9','Nataly Fernandez Morales','OIRS','','','NFM','Fernandez Morales, Nataly Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13455260-3','Ricardo Figueroa Fuentes','Jurídico','-','','RFF','Figueroa Fuentes, Ricardo Antonio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12835652-5','Maria Flanega Palma','Administración y Finanzas','Prevención de Riesgos','','MFP','Flanega Palma, Maria Jose');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12911214-K','Paula Flores Fredes','Jurídico','-','','PFF','Flores Fredes, Paula');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16434060-0','Constanza Flores Piña','Técnico','Campamentos y regeneración de condominios sociales','','CFP','Flores Piña, Constanza');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('5199090-0','Patricia Fuentes Eldan','Jurídico','-','','PFE','Fuentes Eldan, Patricia Eugenia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16881743-6','Lorena Fuentes Gonzalez','Técnico','Obras habitacionales y asistencia técnica','','LFG','Fuentes Gonzalez, Lorena Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12470067-1','Luis Fuenzalida Espinosa','Programación Física y Control','-','','LFE','Fuenzalida Espinosa, Luis Leonardo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17288620-5','Fabiola Gaete Pino','Técnico','Arrastre y coordinación de EGIS SERVIU','','FGP','Gaete Pino, Fabiola De Los Santos');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15525121-2','Maria Gajardo Ortega','Técnico','Campamentos y regeneración de condominios sociales','','MGO','Gajardo Ortega, Maria Paulina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15497350-8','Karla Galarce Galarce','Delegación Provincial Colchagua','-','','KGG','Galarce Galarce, Karla Clarita');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9346293-9','Doris Galdames Toro','Administración y Finanzas','-','','DGT','Galdames Toro, Doris');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13179455-K','Yenny Galleguillos Rodriguez','Técnico','Obras Civiles','','YGR','Galleguillos Rodriguez, Yenny Marcela');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10026795-0','Fernando Galvez Vidal','Técnico','Obras habitacionales y asistencia técnica','','FGV','Galvez Vidal, Fernando');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15115109-4','Carlos Galvez Videla','Dirección Serviu VI','','','CGV','Galvez Videla, Carlos Andres');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14206533-9','Carolina Garrido Molina','Programación Física y Control','-','','CGM','Garrido Molina, Carolina Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13619727-4','Mariela Garrido Molina','Técnico','Arrastre y coordinación de EGIS SERVIU','','MGM','Garrido Molina, Mariela');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10676398-4','Patricia Gibson Marticorena','Técnico','Evaluación de proyectos y programa de mejoramiento de vivindas y barrios','','PGM','Gibson Marticorena, Patricia Magdalena');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10089892-6','Ricardo Gonzalez Campos','Técnico','-','','RGC','Gonzalez Campos, Ricardo Antonio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16648104-K','Elizabeth Gonzalez Cayuqueo','Operaciones Habitacionales','Pagos de Subsidios','','EGC','Gonzalez Cayuqueo, Elizabeth Romina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12642366-7','Margarita Gonzalez Gorigoitia','Administración y Finanzas','-','','MGG','Gonzalez Gorigoitia, Margarita Solange');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('18040755-3','Carla Gonzalez Iturra','Departamento Operaciones Habitacionales','','','CGI','Gonzalez Iturra, Carla Soledad');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16254988-K','Fanny Gonzalez Moraga','OIRS','','','FGM','Gonzalez Moraga, Fanny Del Rosario');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10027038-2','Erika Gonzalez Pavez','Departamento Operaciones Habitacionales','','','EGP','Gonzalez Pavez, Erika');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11671641-0','Paola Gonzalez Ross','Seccion Subsidios','','','PGR','Gonzalez Ross, Paola');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13600109-4','Claudia Gonzalez Silva','Departamento Operaciones Habitacionales','','','CGS','Gonzalez Silva, Claudia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17930784-7','Rodrigo Guajardo Farias','Unidad  Serv. Generales','','','RGF','Guajardo Farias, Rodrigo Andres');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11671060-9','Viviana Guajardo Ramirez','Departamento Tecnico','','','VGR','Guajardo Ramirez, Viviana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13021019-8','Veronica Guardia Camblor','Departamento Tecnico','','','VGC','Guardia Camblor, Veronica Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15125812-3','Loreto Guerra Herrera','Departamento Tecnico','','','LGH','Guerra Herrera, Loreto Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17134105-1','Matias Guerrero Ubilla','Externo','','','MGU','Guerrero Ubilla, Matias Leonardo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14520847-5','Natalia Henriquez Bustos','Dirección Serviu VI','','','NHB','Henriquez Bustos, Natalia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13633222-8','Pamela Hernandez Marambio','Departamento Operaciones Habitacionales','','','PHM','Hernandez Marambio, Pamela');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13602198-2','Ingrid Higuera Gonzalez','Dirección Serviu VI','','','IHG','Higuera Gonzalez, Ingrid');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('18043309-0','Nathaly Huerta Cornejo','Dirección Serviu VI','','','NHC','Huerta Cornejo, Nathaly Jacqueline');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16973751-7','Tamara Ibarra Ibarra','Delegación Provincial Colchagua','','','TII','Ibarra Ibarra, Tamara Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11365581-K','Albania Jara Diaz','Unidad  Contab. y Ppto.','','','AJD','Jara Diaz, Albania');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13718156-8','Daniela Jara Soto','Departamento Jurídico','','','DJS','Jara Soto, Daniela Francisca');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12911101-1','Marcela Jimenez Gatica','Departamento Operaciones Habitacionales','','','MJG','Jimenez Gatica, Marcela Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15117763-8','Karin Jiroz Burgos','Departamento Operaciones Habitacionales','','','KJB','Jiroz Burgos, Karin');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16765192-5','Astrid Lagos Castro','Unidad  Contab. y Ppto.','','','ALC','Lagos Castro, Astrid Escarlette');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15430941-1','Araceli Landini Cavieres','Departamento Operaciones Habitacionales','','','ALC','Landini Cavieres, Araceli');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15112654-5','Maria Latorre Escandon','Departamento Tecnico','','','MLE','Latorre Escandon, Maria De Los Angeles');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15954544-K','Roxana Leal Vidal','Honorario Subsecret. SERVIU VI','','','RLV','Leal Vidal, Roxana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17521407-0','Ana Leon Escobedo','Externo','','','ALE','Leon Escobedo, Ana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11981726-9','Robinson Leon Herrera','Unidad  Ingenieria y Vialidad Urb','','','RLH','Leon Herrera, Robinson');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9573985-7','Georgina Lillo Tobar','Departamento Jurídico','','','GLT','Lillo Tobar, Georgina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13895079-4','Paula Lobos Ríos','Honorario Subsecret. SERVIU VI','','','PLR','Lobos Ríos, Paula');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13023525-5','Karen Lopez Hernandez','Departamento Tecnico','','','KLH','Lopez Hernandez, Karen Gloria');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14284856-2','Magaly Madriaga Rojas','Delegación Provincial Colchagua','','','MMR','Madriaga Rojas, Magaly');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8324515-8','Andres Madrid Garcia','Unidad  Serv. Generales','','','AMG','Madrid Garcia, Andres Felipe');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11757569-1','Paola Maldonado Caroca','Dirección Serviu VI','','','PMC','Maldonado Caroca, Paola');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('5610738-K','Elsa Mantelli Ruiz','Departamento Operaciones Habitacionales','','','EMR','Mantelli Ruiz, Elsa Velia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16164935-K','Evelyn Marilaf Romo','Unidad  Contab. y Ppto.','','','EMR','Marilaf Romo, Evelyn Eliana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15345802-2','John Martin Romero','Unidad  Serv. Generales','','','JMR','Martin Romero, John Steve');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15305388-K','Jorge Martinez Alfaro','Unidad  Ingenieria y Vialidad Urb','','','JMA','Martinez Alfaro, Jorge Gustavo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15668690-5','Jose Martinez Saldias','Departamento Adm. y Finanzas','','','JMS','Martinez Saldias, Jose Alberto');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15523703-1','Maria Medina Bravo','Unidad  Estudios y Proyectos','','','MMB','Medina Bravo, Maria Rosario');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17523596-5','Sandy Medina Leiva','Dirección Serviu VI','','','SML','Medina Leiva, Sandy Estefania');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6601737-0','Ana Meza Zuñiga','Unidad  Recursos Humanos','','','AMZ','Meza Zuñiga, Ana Luisa');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16223615-6','Luis Migueles Cornejo','Unidad  Recursos Humanos','','','LMC','Migueles Cornejo, Luis Felipe');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8341242-9','Jose Mingorance Quezada','Departamento Prog. Y Control','','','JMQ','Mingorance Quezada, Jose Andres');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6855008-4','Mercedes Miranda Araya','Dirección Serviu VI','','','MMA','Miranda Araya, Mercedes Elen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16070527-2','Felipe Mix Fuentes','Unidad  Serv. Generales','','','FMF','Mix Fuentes, Felipe Nicolas');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6957739-3','Maria Montaño Villegas','Unidad  Obras de Pavimentacion','','','MMV','Montaño Villegas, Maria Aida');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14203429-8','Carlo Montre Diaz','Departamento Tecnico','','','CMD','Montre Diaz, Carlos German');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9843688-K','Joel Moraga Fernandez','Dirección Serviu VI','','','JMF','Moraga Fernandez, Joel');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11760385-7','Olivia Morales Farias','Unidad  Serv. Generales','','','OMF','Morales Farias, Olivia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6028630-2','Walter Morales Rojo','Unidad  Obras de Pavimentacion','','','WMR','Morales Rojo, Walter');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15106771-9','Fabiola Moreno Cantillana','Departamento Tecnico','','','FMC','Moreno Cantillana, Fabiola Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14485955-3','Leticia Moya Silva','Seccion Subsidios','','','LMS','Moya Silva, Leticia Agustina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15093222-K','Maudy Muñoz Astorga','Seccion Asig. y Postulaciones','','','MMA','Muñoz Astorga, Maudy Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('18040012-5','Danitza Muñoz Muñoz','OIRS','','','DMM','Muñoz Muñoz, Danitza Pilar');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15994718-1','Paulina Muñoz Sepulveda','Delegación Provincial Colchagua','','','PMS','Muñoz Sepulveda, Paulina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10132279-3','Marcela Muñoz Valerio','Delegación Provincial Colchagua','','','MMV','Muñoz Valerio, Marcela Liliana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14199907-9','Rosa Nuñez Bustamante','Departamento Operaciones Habitacionales','','','RNB','Nuñez Bustamante, Rosa Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13946536-9','René Nuñez Miranda','Departamento Tecnico','','','RNM','Nuñez Miranda, René Luis');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8069594-2','Sonia Ochoa Fuenzalida','Departamento Jurídico','','','SOF','Ochoa Fuenzalida, Sonia Miri');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15303342-0','Jenny Ojeda Ovando','Departamento Jurídico','','','JOO','Ojeda Ovando, Jenny Elizabeth');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15434150-1','Daniel Olate Reyes','Departamento Jurídico','','','DOR','Olate Reyes, Daniel Alonso');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15126332-1','Roberto Olea Leon','OIRS','','','ROL','Olea Leon, Roberto Mauricio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13780275-9','Jorge Orellana Henriquez','Dirección Serviu VI','','','JOH','Orellana Henriquez, Jorge Luis');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16835263-8','Rosario Orellana Silva','Dirección Serviu VI','','','ROS','Orellana Silva, Rosario Del Carmen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('18043742-8','Felipe Osses Garcia','Externo','','','FOG','Osses Garcia, Felipe Amador');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16255416-6','Nataly Oyarzo Solis','Delegación Provincial Colchagua','','','NOS','Oyarzo Solis, Nataly Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15125532-9','Paulina Pacheco Pastén','Dirección Serviu VI','','','PPP','Pacheco Pastén, Paulina Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10049931-2','Sergio Pardo Zuñiga','Unidad  Obras de Pavimentacion','','','SPZ','Pardo Zuñiga, Sergio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16165151-6','Natalia Parga Contreras','Unidad  Recursos Humanos','','','NPC','Parga Contreras, Natalia Valesca');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13474439-1','Estela Parra González','Departamento Tecnico','','','EPG','Parra González, Estela Amada');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16491460-7','Carla Parraguez Parrao','Departamento Tecnico','','','CPP','Parraguez Parrao, Carla Cristy');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13944222-9','Mary Paz Cerda','Departamento Tecnico','','','MPC','Paz Cerda, Mary Helen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('7721708-8','Ivett Peña Baez','Unidad  Contab. y Ppto.','','','IPB','Peña Baez, Ivett');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16159749-K','Javier Peralta Correa','Unidad  Ingenieria y Vialidad Urb','','','JPC','Peralta Correa, Javier Leandro');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6333540-1','Luciano Perez Cid','Departamento Jurídico','','','LPC','Perez Cid, Luciano');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14577637-6','Andrea Perez Gonzalez','Dirección Serviu VI','','','APG','Perez Gonzalez, Andrea Clementina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17527388-3','Jessica Perez Palma','Departamento Tecnico','','','JPP','Perez Palma, Jessica Nicole');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9324204-1','Rigoberto Pino Garay','Unidad  Estudios y Proyectos','','','RPG','Pino Garay, Rigoberto Del Carmen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17746670-0','Rodrigo Pino Loayza','Delegación Provincial Colchagua','','','RPL','Pino Loayza, Rodrigo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14011661-0','Carlos Pinto Miranda','Departamento Adm. y Finanzas','','','CPM','Pinto Miranda, Carlos Alberto');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11274007-4','Yasna Poblete Galarce','Departamento Tecnico','','','YPG','Poblete Galarce, Yasna Angelica');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17156488-3','Francisco Poblete Guerra','Departamento Prog. Y Control','','','FPG','Poblete Guerra, Francisco Javier');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14525975-4','Liliana Pulgar Tobar','Departamento Prog. Y Control','','','LPT','Pulgar Tobar, Liliana Del Carmen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13282108-9','Pabla Quevedo Tapia','OIRS','','','PQT','Quevedo Tapia, Pabla Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9071845-2','David Quintana Riffo','Unidad  Obras de Pavimentacion','','','DQR','Quintana Riffo, David');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15104348-8','Julia Ramirez Vega','Departamento Tecnico','','','JRV','Ramirez Vega, Julia De Lourdes');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16846148-8','Barbara Retamal Farias','Unidad  Partes','','','BRF','Retamal Farias, Barbara Maria');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14024158-K','Esteban Reyes Arriagada','Dirección Serviu VI','','','ERA','Reyes Arriagada, Esteban Eduardo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17505351-4','Ivette Reyes Campos','Dirección Serviu VI','','','IRC','Reyes Campos, Ivette Angelina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11593521-6','Dagoberto Reyes Gallardo','Unidad  Estudios y Proyectos','','','DRG','Reyes Gallardo, Dagoberto Fortuoso');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11889092-2','Claudio Reyes Morales','Departamento Adm. y Finanzas','','','CRM','Reyes Morales, Claudio Ivan');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11888814-6','Dania Ríos Donoso','Unidad  Contab. y Ppto.','','','DRD','Ríos Donoso, Dania Marlene');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11919911-5','Andres Rioseco Sanchez','Departamento Tecnico','','','ARS','Rioseco Sanchez, Andres Gonzalo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11670393-9','Marisol Riquelme Jiron','Dirección Serviu VI','','','MRJ','Riquelme Jiron, Marisol');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13857364-8','Maria Riquelme Verdugo','Honorario Subsecret. SERVIU VI','','','MRV','Riquelme Verdugo, Maria Daniella');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16988669-5','Joaquin Rodriguez Benitez','Unidad  Ingenieria y Vialidad Urb','','','JRB','Rodriguez Benitez, Joaquin Alejandro');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9782047-3','Claudia Rodriguez Briceño','Departamento Jurídico','','','CRB','Rodriguez Briceño, Claudia Helia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15993786-0','Dara Rodriguez Rodriguez','Seccion Subsidios','','','DRR','Rodriguez Rodriguez, Dara Karina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12690664-1','Veronica Rojas Faundez','Departamento Operaciones Habitacionales','','','VRF','Rojas Faundez, Veronica');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14054963-0','Gredy Roldan Hernandez','Dirección Serviu VI','','','GRH','Roldan Hernandez, Gredy Lorena');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16913909-1','Rosa Roman Molina','Unidad  Contab. y Ppto.','','','RRM','Roman Molina, Rosa Esther');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11792384-3','Manuel Rosales Pino','Departamento Adm. y Finanzas','','','MRP','Rosales Pino, Manuel Francisco');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12981190-0','Paulina Rosales Pino','Departamento Prog. Y Control','','','PRP','Rosales Pino, Paulina Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14556920-6','Andrea Rozas Guajardo','Delegación Provincial Colchagua','','','ARG','Rozas Guajardo, Andrea Eliana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12517221-0','Gabriela Rubio Donoso','Departamento Operaciones Habitacionales','','','GRD','Rubio Donoso, Gabriela Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15106920-7','Cristian Rubio Nuñez','Unidad  Serv. Generales','','','CRN','Rubio Nuñez, Cristian');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17527595-9','Yasna Rubio Nuñez','Departamento Operaciones Habitacionales','','','YRN','Rubio Nuñez, Yasna Katiuska');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14261463-4','Karla Ruz Guerra','Dirección Serviu VI','','','KRG','Ruz Guerra, Karla Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15994654-1','Lorena Saez Muñoz','Dirección Serviu VI','','','LSM','Saez Muñoz, Lorena Pilar');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6291247-2','Alejandro Salazar Cerda','Dirección Serviu VI','','','ASC','Salazar Cerda, Alejandro Enr');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12040442-3','Alejandra Salazar Oyarzun','Departamento Jurídico','','','ASO','Salazar Oyarzun, Alejandra Maria');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14200653-7','Enrique Salazar Oyarzun','Unidad  Partes','','','ESO','Salazar Oyarzun, Enrique Felipe');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11759831-4','Rodrigo Salazar Perez','Dirección Serviu VI','','','RSP','Salazar Perez, Rodrigo Fernando');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11398314-0','Mirta Sanchez Acevedo','Departamento Jurídico','','','MSA','Sanchez Acevedo, Mirta');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9025800-1','Maria Sanchez Letelier','Delegación Provincial Colchagua','','','MSL','Sanchez Letelier, Maria Del');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13095884-2','Veronica Santelices Iriarte','Departamento Tecnico','','','VSI','Santelices Iriarte, Veronica Hada');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12367094-9','Victor Sepulveda Becerra','Honorario Subsecret. SERVIU VI','','','VSB','Sepulveda Becerra, Victor');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13905040-1','Luis Sepulveda Guerrero','Departamento Tecnico','','','LSG','Sepulveda Guerrero, Luis Rodolfo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8754539-3','Pedro Sepulveda Parraguez','Unidad  Serv. Generales','','','PSP','Sepulveda Parraguez, Pedro');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16250630-7','Nathaly Seura Sandoval','Departamento Operaciones Habitacionales','','','NSS','Seura Sandoval, Nathaly Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13373369-8','Carolina Sobarzo Marquez','Departamento Prog. Y Control','','','CSM','Sobarzo Marquez, Carolina Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13500805-2','Carlos Solis Cerda','Departamento Jurídico','','','CSC','Solis Cerda, Carlos Roberto');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15738442-2','Carlo Solis Quezada','Departamento Jurídico','','','CSQ','Solis Quezada, Carlo Alejandro');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15123528-K','Karla Solis Quezada','Departamento Operaciones Habitacionales','','','KSQ','Solis Quezada, Karla');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8177289-4','Erna Soto Donoso','Unidad  Contab. y Ppto.','','','ESD','Soto Donoso, Erna Viviana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8107726-6','Purisima Soto Ponce','Unidad  Ingenieria y Vialidad Urb','','','PSP','Soto Ponce, Purisima Del C.');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13636135-K','Lidia Tapia Giovanetti','Departamento Tecnico','','','LTG','Tapia Giovanetti, Lidia Paola');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('12516949-K','Marcelo Tarifeño Zarate','Unidad  Serv. Generales','','','MTZ','Tarifeño Zarate, Marcelo Hernan');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15755889-7','Constanza Tello Salgado','Externo','','','CTS','Tello Salgado, Constanza');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15123499-2','Francisco Torneria Torres','Departamento Tecnico','','','FTT','Torneria Torres, Francisco Javier');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13251521-2','Juan Toro Flores','Dirección Serviu VI','','','JTF','Toro Flores, Juan Pablo');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15103780-1','Hellen Toro Perez','Departamento Jurídico','','','HTP','Toro Perez, Hellen Joice');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8458396-0','Rodrigo Triviño Fernandez','Departamento Operaciones Habitacionales','','','RTF','Triviño Fernandez, Rodrigo Marcel');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17857298-9','Camila Troncoso Correa','Externo','','','CTC','Troncoso Correa, Camila');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('18521882-1','Manuel Urra Serrano','Externo','','','MUS','Urra Serrano, Manuel');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9654311-5','Luis Urzua Zurita','Unidad  Obras de Pavimentacion','','','LUZ','Urzua Zurita Luis');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10933738-2','Francisco Valdes Flores','Departamento Prog. Y Control','','','FVF','Valdes Flores, Francisco Antonio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15804485-4','Nelson Valdes Vargas','Departamento Adm. y Finanzas','','','NVV','Valdes Vargas, Nelson Felipe');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10499632-9','Ivan Valdivia Flores','Delegación Provincial Colchagua','','','IVF','Valdivia Flores, Ivan');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13300722-9','Roberto Valdivia Garcia','Unidad  Obras de Pavimentacion','','','RVG','Valdivia Garcia, Roberto Anibal');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('16189695-0','Johana Valdivia Valdivia','Departamento Jurídico','','','JVV','Valdivia Valdivia, Johana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6283645-8','Juan Valencia Andrades','Unidad  Ingenieria y Vialidad Urb','','','JVA','Valencia Andrades, Juan Manu');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('10743908-0','Mireya Valencia Gallardo','Unidad  Estudios y Proyectos','','','MVG','Valencia Gallardo, Mireya Sofia');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13944325-K','Valeria Valenzuela Jara','Departamento Operaciones Habitacionales','','','VVJ','Valenzuela Jara, Valeria Del Carmen');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13775901-2','Christian Valenzuela Moreno','Unidad  Ingenieria y Vialidad Urb','','','CVM','Valenzuela Moreno, Christian');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6569309-7','Rosa Valenzuela Poblete','Dirección Serviu VI','','','RVP','Valenzuela Poblete, Rosa E.');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17137779-K','Alejandra Valenzuela Rodriguez','Departamento Adm. y Finanzas','','','AVR','Valenzuela Rodriguez, Alejandra Noemi');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15117095-1','Paulina Valenzuela Valdes','Dirección Serviu VI','','','PVV','Valenzuela Valdes, Paulina Alejandra');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17507852-5','Alvaro Vasques Cespedes','Externo','','','AVC','Vasques Cespedes, Alvaro');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('13257445-6','Paulina Vasquez Yung','Departamento Tecnico','','','PVY','Vasquez Yung, Paulina');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('6825927-4','Adolfo Vega Araya','Unidad  Ingenieria y Vialidad Urb','','','AVA','Vega Araya, Adolfo Emilio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15139983-5','Daniela Veliz Rebolledo','Departamento Operaciones Habitacionales','','','DVR','Veliz Rebolledo, Daniela Andrea');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('11132609-6','Ana Vidal Jilberto','Departamento Tecnico','','','AVJ','Vidal Jilberto, Ana Maria');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15104497-2','Patricio Vidal Orellana','Unidad  Contab. y Ppto.','','','PVO','Vidal Orellana, Patricio Hermogenes');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15122646-9','Michelle Vidal Reyes','Departamento Operaciones Habitacionales','','','MVR','Vidal Reyes, Michelle Melissa');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8146942-3','Ana Viera Cabezas','Unidad  Contab. y Ppto.','','','AVC','Viera Cabezas, Ana');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8303544-7','Cristian Vilches Estrada','Unidad  Serv. Generales','','','CVE','Vilches Estrada, Cristian Rafael');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('5893753-3','Gabriela Villalon Alfaro','Departamento Operaciones Habitacionales','','','GVA','Villalon Alfaro, Gabriela');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('14011541-K','Marco Villanueva Garrido','Departamento Tecnico','','','MVG','Villanueva Garrido, Marco Antonio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('8392282-6','Julio Villarroel Yon','Dirección Serviu VI','','','JVY','Villarroel Yon, Julio Ernesto');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('17887134-K','Daniel Villaseca Cuevas','Departamento Tecnico','','','DVC','Villaseca Cuevas, Daniel Christian');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('9868840-4','Esteban Zamora Chavez','Departamento Prog. Y Control','','','EZC','Zamora Chavez, Esteban Sergio');
INSERT INTO `mobiliarioServiu`.`funcionario`
(
`rut`,
`nombre`,
`depto`,
`unidad`,
`cargo`,
`iniciales`,
`apellidoNombre`)
VALUES
('15108097-9','Daniela Zuñiga Castro','Seccion Subsidios','','','DZC','Zuñiga Castro, Daniela Giovanna');