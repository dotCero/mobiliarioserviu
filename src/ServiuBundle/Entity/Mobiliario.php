<?php

namespace ServiuBundle\Entity;

/**
 * Mobiliario
 */
class Mobiliario {

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $observacion;

    /**
     * @var integer
     */
    private $id;

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Mobiliario
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Mobiliario
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return Mobiliario
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion() {
        return $this->observacion;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @var integer
     */
    private $valor;

    /**
     * @var \Date
     */
    private $fechaadquisicion;

    /**
     * @var \Date
     */
    private $vidautil;

    /**
     * @var integer
     */
    private $depreciacion;

    /**
     * @var integer
     */
    private $valorresidual;

    /**
     * @var integer
     */
    private $valoractualizado;

    /**
     * Set valor
     *
     * @param integer $valor
     *
     * @return Mobiliario
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return integer
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set fechaadquisición
     *
     * @param \Date $fechaadquisicion
     *
     * @return Mobiliario
     */
    public function setFechaadquisicion($fechaadquisicion) {
        $this->fechaadquisicion = $fechaadquisicion;

        return $this;
    }

    /**
     * Get fechaadquisición
     *
     * @return \Date
     */
    public function getFechaadquisicion() {
        return $this->fechaadquisicion;
    }

    /**
     * Set vidautil
     *
     * @param \Date $vidautil
     *
     * @return Mobiliario
     */
    public function setVidautil($vidautil) {
        $this->vidautil = $vidautil;

        return $this;
    }

    /**
     * Get vidautil
     *
     * @return \Date
     */
    public function getVidautil() {
        return $this->vidautil;
    }

    /**
     * Set depreciacion
     *
     * @param integer $depreciacion
     *
     * @return Mobiliario
     */
    public function setDepreciacion($depreciacion) {
        $this->depreciacion = $depreciacion;

        return $this;
    }

    /**
     * Get depreciacion
     *
     * @return integer
     */
    public function getDepreciacion() {
        return $this->depreciacion;
    }

    /**
     * Set valorresidual
     *
     * @param integer $valorresidual
     *
     * @return Mobiliario
     */
    public function setValorresidual($valorresidual) {
        $this->valorresidual = $valorresidual;

        return $this;
    }

    /**
     * Get valorresidual
     *
     * @return integer
     */
    public function getValorresidual() {
        return $this->valorresidual;
    }

    /**
     * Set valoractualizado
     *
     * @param integer $valoractualizado
     *
     * @return Mobiliario
     */
    public function setValoractualizado($valoractualizado) {
        $this->valoractualizado = $valoractualizado;

        return $this;
    }

    /**
     * Get valoractualizado
     *
     * @return integer
     */
    public function getValoractualizado() {
        return $this->valoractualizado;
    }

    public function __toString() {
        return $this->codigo." - ".$this->observacion;
    }
}