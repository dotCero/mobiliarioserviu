<?php

namespace ServiuBundle\Entity;

/**
 * Funcionario
 */
class Funcionario {



    /**
     * @var string
     */
    private $rut;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $depto;

    /**
     * @var string
     */
    private $unidad;

    /**
     * @var string
     */
    private $cargo;

    /**
     * @var string
     */
    private $iniciales;

    /**
     * @var integer
     */
    private $id;

    /**
     * Set rut
     *
     * @param string $rut
     *
     * @return Funcionario
     */
    public function setRut($rut) {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string
     */
    public function getRut() {
        return $this->rut;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Funcionario
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set depto
     *
     * @param string $depto
     *
     * @return Funcionario
     */
    public function setDepto($depto) {
        $this->depto = $depto;

        return $this;
    }

    /**
     * Get depto
     *
     * @return string
     */
    public function getDepto() {
        return $this->depto;
    }

    /**
     * Set unidad
     *
     * @param string $unidad
     *
     * @return Funcionario
     */
    public function setUnidad($unidad) {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return string
     */
    public function getUnidad() {
        return $this->unidad;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     *
     * @return Funcionario
     */
    public function setCargo($cargo) {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo() {
        return $this->cargo;
    }

    /**
     * Set iniciales
     *
     * @param string $iniciales
     *
     * @return Funcionario
     */
    public function setIniciales($iniciales) {
        $this->iniciales = $iniciales;

        return $this;
    }

    /**
     * Get iniciales
     *
     * @return string
     */
    public function getIniciales() {
        return $this->iniciales;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function __toString() {
        return $this->rut." - ".$this->nombre;
    }

}
