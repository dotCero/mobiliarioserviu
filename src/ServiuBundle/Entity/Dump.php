<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ServiuBundle\Entity;

/**
 * Description of Dump
 *
 * @author cero
 */
class Dump {
    private $dump;

    /**
     * Dump constructor.
     * @param $dump
     */
    public function __construct($dump)
    {
        $this->dump = $dump;
    }


    /**
     * Get dump
     *
     * @return string
     */
    public function getDump() {
        return $this->dump;
    }
    /**
     * Set dump
     *
     * @param string $dump
     *
     * @return Dump
     */
    public function setDump($dump) {
        $this->dump = $dump;
    }


}
