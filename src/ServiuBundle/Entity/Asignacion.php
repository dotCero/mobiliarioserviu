<?php

namespace ServiuBundle\Entity;

/**
 * Asignacion
 */
class Asignacion {

    /**
     * @var integer
     */
    private $codigoasignacion;

    /**
     * @var integer
     */
    private $codigobarras;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \ServiuBundle\Entity\Funcionario
     */
    private $idfuncionario;

    /**
     * @var \ServiuBundle\Entity\Mobiliario
     */
    private $idmobiliario;
    /**
     * Set codigoasignacion
     *
     * @param integer $codigoasignacion
     *
     * @return Asignacion
     */
    public function setCodigoasignacion($codigoasignacion) {
        $this->codigoasignacion = $codigoasignacion;

        return $this;
    }

    /**
     * Get codigoasignacion
     *
     * @return integer
     */
    public function getCodigoasignacion() {
        return $this->codigoasignacion;
    }

    /**
     * Set codigobarras
     *
     * @param integer $codigobarras
     *
     * @return Asignacion
     */
    public function setCodigobarras($codigobarras) {
        $this->codigobarras = $codigobarras;

        return $this;
    }

    /**
     * Get codigobarras
     *
     * @return integer
     */
    public function getCodigobarras() {
        return $this->codigobarras;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set idfuncionario
     *
     * @param \ServiuBundle\Entity\Funcionario $idfuncionario
     *
     * @return Asignacion
     */
    public function setIdfuncionario(\ServiuBundle\Entity\Funcionario $idfuncionario = null) {
        $this->idfuncionario = $idfuncionario;

        return $this;
    }

    /**
     * Get idfuncionario
     *
     * @return \ServiuBundle\Entity\Funcionario
     */
    public function getIdfuncionario() {
        return $this->idfuncionario;
    }

    /**
     * Set idmobiliario
     *
     * @param \ServiuBundle\Entity\Mobiliario $idmobiliario
     *
     * @return Asignacion
     */
    public function setIdmobiliario(\ServiuBundle\Entity\Mobiliario $idmobiliario = null) {
        $this->idmobiliario = $idmobiliario;

        return $this;
    }

    /**
     * Get idmobiliario
     *
     * @return \ServiuBundle\Entity\Mobiliario
     */
    public function getIdmobiliario() {
        return $this->idmobiliario;
    }

    public function __toString() {
        return $this->codigobarras;
    }
    /**
     * @var integer
     */
    private $cantidadmobiliario;


    /**
     * Set cantidadmobiliario
     *
     * @param integer $cantidadmobiliario
     *
     * @return Asignacion
     */
    public function setCantidadmobiliario($cantidadmobiliario)
    {
        $this->cantidadmobiliario = $cantidadmobiliario;

        return $this;
    }

    /**
     * Get cantidadmobiliario
     *
     * @return integer
     */
    public function getCantidadmobiliario()
    {
        return $this->cantidadmobiliario;
    }
}
