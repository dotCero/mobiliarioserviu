<?php

namespace ServiuBundle\Entity;

/**
 * Clase creada para darle forma a custom query para armar reporte
 */
class Reporte {

    private $codigo;
    private $nombre;
    private $cantidad;
    private $valor;

    /**
     * @var \Date
     */
    private $fechaAdquisicion;

    /**
     * @var \Date
     */
    private $vidaUtil;
    private $depreciacion;
    private $valorResidual;
    private $valorActualizado;

    public function getCodigo() {
        return $this->codigo;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function getValor() {
        return $this->valor;
    }

    /**
     * Get fechaAdquisición
     *
     * @return \Date
     */
    public function getFechaAdquisicion() {
        return $this->fechaAdquisicion;
    }

    /**
     * Get vidaUtil
     *
     * @return \Date
     */
    public function getVidaUtil() {
        return $this->vidaUtil;
    }

    public function getDepreciacion() {
        return $this->depreciacion;
    }

    public function getValorResidual() {
        return $this->valorResidual;
    }

    public function getValorActualizado() {
        return $this->valorActualizado;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function setFechaAdquisicion($fechaAdquisicion) {
        $this->fechaAdquisicion = $fechaAdquisicion;
    }

    public function setVidaUtil($vidaUtil) {
        $this->vidaUtil = $vidaUtil;
    }

    public function setDepreciacion($depreciacion) {
        $this->depreciacion = $depreciacion;
    }

    public function setValorResidual($valorResidual) {
        $this->valorResidual = $valorResidual;
    }

    public function setValorActualizado($valorActualizado) {
        $this->valorActualizado = $valorActualizado;
    }
}