<?php

namespace ServiuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AsignacionType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('idfuncionario', EntityType::class, ['label' => 'Asignar a', 'class' => 'ServiuBundle:Funcionario', 'placeholder' => 'Seleccionar...', 'required' => true, 'attr' => ['onclick' => 'cBarras()']])
                ->add('idmobiliario', EntityType::class, ['label' => 'Asignar', 'class' => 'ServiuBundle:Mobiliario', 'placeholder' => 'Seleccionar...', 'required' => true, 'attr' => ['onclick' => 'cBarras()']])
                ->add('codigoasignacion', NumberType::class, ['label' => 'Código Asignación: ', 'required' => true, 'attr' => ['onkeyup' => 'cBarras()']])
                ->add('codigobarras', TextType::class, ['label' => 'Código de Barras', 'required' => true, 'attr' => ['onkeyup' => 'cBarras()', 'readonly' => 'readonly', 'placeholder' => ' ']])
                ->add('cantidadmobiliario', NumberType::class, ['label' => 'Cantidad', 'required' => true, 'attr' => ['onkeyup' => 'cBarras()']])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ServiuBundle\Entity\Asignacion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'serviubundle_asignacion';
    }

}
