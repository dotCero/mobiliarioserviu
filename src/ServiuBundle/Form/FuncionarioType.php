<?php

namespace ServiuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FuncionarioType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('rut', TextType::class, [
                    'label' => 'Rut',
                    'required' => true,
                    'attr' => [
                        'maxlength' => '10',
                    ],
                ])
                ->add('nombre', TextType::class, [
                    'label' => 'Nombre',
                    'required' => true,
                    'attr' => [
                        'onkeyup' => 'rellenar()',
                        'autocomplete' => 'off',
                        'disabled' => true,
                    ],
                ])
                ->add('depto', TextType::class, [
                    'label' => 'Departamento',
                    'required' => true,
                    'attr' => [
                        'disabled' => true,
                    ]
                ])
                ->add('unidad', TextType::class, [
                    'label' => 'Unidad',
                    'required' => true,
                    'attr' => [
                        'disabled' => true,
                    ],
                ])
                ->add('cargo', TextType::class, [
                    'label' => 'Cargo',
                    'required' => true,
                    'attr' => [
                        'disabled' => true,
                    ],
                ])
                ->add('iniciales', TextType::class, ['label' => 'Iniciales', 'required' => false, 'attr' => ['readonly' => 'readonly', 'placeholder' => ' ']])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ServiuBundle\Entity\Funcionario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'serviubundle_funcionario';
    }

}
