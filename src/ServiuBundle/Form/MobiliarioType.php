<?php

namespace ServiuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class MobiliarioType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('codigo', TextType::class, [
                    'label' => 'Código',
                    'required' => true
                    ])
                
                ->add('nombre', TextType::class, [
                    'label' => 'Nombre',
                    'required' => true
                    ])
                
                ->add('observacion', TextareaType::class, [
                    'label' => 'Observación',
                    'required' => true,
                    'attr' => [
                        'class' => 'materialize-textarea'
                        ],
                    ])
                
                ->add('valor', NumberType::class, [
                    'label' => 'Valor $',
                    'required' => true
                    ])
                
                ->add('fechaadquisicion', DateType::class, [
                    'label' => 'Fecha De Adquisición',
                    'required' => true,
                    'widget' => 'single_text',
                    'attr' => [
                        'class' => 'datepicker'
                    ],
                ])
                
                ->add('vidautil', DateType::class, [
                    'label' => 'Vida Útil',
                    'required' => true,
                    'widget' => 'single_text',
                    'attr' => [
                        'class' => 'datepicker'
                    ],
                ])
                
                ->add('depreciacion', NumberType::class, [
                    'label' => 'Depreciación',
                    'required' => true
                    ])
                
                ->add('valorresidual', NumberType::class, [
                    'label' => 'Valor Residual',
                    'required' => true
                    ])
                
                ->add('valoractualizado', NumberType::class, [
                    'label' => 'Valor Actualizado',
                    'required' => true
                    ])
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ServiuBundle\Entity\Mobiliario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'serviubundle_mobiliario';
    }

}
