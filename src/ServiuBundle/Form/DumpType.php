<?php

namespace ServiuBundle\Form;

use ServiuBundle\Controller\LoadDumps;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Description of DumpType
 *
 * @author cero
 */
class DumpType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'chooser',
                ChoiceType::class,
                ['choices' => $this->getList(), 'label' => 'Escoger Dump']
            )
        /*->add(
                'restore',
                ButtonType::class,
                ['label' => 'Restaurar']
            )*/;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ServiuBundle\Entity\Dump'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'serviubundle_dump';
    }

    public function getList()
    {
        $dumps = [];
        $directorio = opendir("/usr/moseDump"); //ruta actual
        if (strpos(PHP_OS, 'WIN')) {
            $directorio = opendir("'C:\MoSeDump'");
        }
        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (is_dir($archivo)) {//verificamos si es o no un directorio
                //de ser un directorio lo envolvemos entre corchetes
            } else {
                $extension = explode(".", $archivo);
                if ($extension[count($extension) - 1] == "sql") {
                    $dumps[$this->formatCombo($archivo)] = $archivo;
                }
            }
        }
        return $dumps;
    }

    private function formatCombo($s)
    {
        $partOne = explode("_", $s);
        $indice = count($partOne) - 1;
        $partTwo = explode(".", $partOne[$indice]);
        $brute = $partTwo[0]; //fecha y horaa

        $separador = [];

        for ($i = 0; $i < strlen($brute); $i++) {
            array_push($separador, $brute[$i]);
        }

        $dia = $separador[0] . $separador[1];
        $mes = $separador[2] . $separador[3];
        $anio = $separador[4] . $separador[5] . $separador[6] . $separador[7];
        $hora = $separador[8] . $separador[9];
        $min = $separador[10] . $separador[11];
        $seg = $separador[12] . $separador[13];

        $fecha = $anio . "-" . $mes . "-" . $dia;
        $tiempo = $hora . ":" . $min . ":" . $seg;

        $full = $fecha . " " . $tiempo;

        $edd = new \DateTimeZone("America/Santiago");

        $date = date_create_from_format("Y-m-d H:i:s", $full, $edd);
        $kk = $date->format("l, d \d\\e F \d\\e\l Y \a \l\a\s H:i:s");

        return $this->toCL($kk);
    }

    private function toCL($dateENG)
    {
        $search = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $replace = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];


        return str_replace($search, $replace, $dateENG);
    }
}