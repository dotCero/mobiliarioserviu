<?php

namespace ServiuBundle\Controller;

use ServiuBundle\Entity\Asignacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Asignacion controller.
 *
 */
class AsignacionController extends Controller {

    /**
     * Lists all asignacion entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $asignaciones = $em->getRepository('ServiuBundle:Asignacion')->findAll();

        return $this->render('asignacion/index.html.twig', array(
                    'asignaciones' => $asignaciones,
        ));
    }

    /**
     * Creates a new asignacion entity.
     *
     */
    public function newAction(Request $request) {
        $asignacion = new Asignacion();
        $form = $this->createForm('ServiuBundle\Form\AsignacionType', $asignacion);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $mobiliarios = $em->getRepository('ServiuBundle:Mobiliario')->findAll();
        $funcionarios = $em->getRepository('ServiuBundle:Funcionario')->findAll();

        if ($form->isSubmitted() && $form->isValid()) {

            $repo = $this->getDoctrine()->getRepository('ServiuBundle:Asignacion');

            $asi = $repo->findOneByCodigoasignacion($asignacion->getCodigoasignacion());

            if ($asi) {
                $this->addFlash("h", $asi->getCodigoasignacion());

                return $this->render('asignacion/new.html.twig', array(
                            'asignacion' => $asignacion,
                            'mobiliarios' => $mobiliarios,
                            'funcionarios' => $funcionarios,
                            'form' => $form->createView(),
                ));
            }

            $em->persist($asignacion);
            $em->flush($asignacion);

            $this->addFlash("m", [$asignacion->getIdmobiliario()->getNombre(), $asignacion->getIdfuncionario()->getNombre()]);

            return $this->redirectToRoute('funcionario_inventario', array('id' => $asignacion->getIdfuncionario()->getId()));
        }

//        $em = $this->getDoctrine()->getManager();
//
//        $mobiliarios = $em->getRepository('ServiuBundle:Mobiliario')->findAll();
//        $funcionarios = $em->getRepository('ServiuBundle:Funcionario')->findAll();

        return $this->render('asignacion/new.html.twig', array(
                    'asignacion' => $asignacion,
                    'mobiliarios' => $mobiliarios,
                    'funcionarios' => $funcionarios,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a asignacion entity.
     *
     */
    public function showAction(Asignacion $asignacion) {
        $deleteForm = $this->createDeleteForm($asignacion);

        return $this->render('asignacion/show.html.twig', array(
                    'asignacion' => $asignacion,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing asignacion entity.
     *
     */
    public function editAction(Request $request, Asignacion $asignacion) {
        $deleteForm = $this->createDeleteForm($asignacion);
        $editForm = $this->createForm('ServiuBundle\Form\AsignacionType', $asignacion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('asignacion_index', array('id' => $asignacion->getId()));
        }

        return $this->render('asignacion/edit.html.twig', array(
                    'asignacion' => $asignacion,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a asignacion entity.
     *
     */
    public function deleteAction(Request $request, Asignacion $asignacion) {
        $form = $this->createDeleteForm($asignacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($asignacion);
            $em->flush($asignacion);
        }

        return $this->redirectToRoute('asignacion_index');
    }

    /**
     * Creates a form to delete a asignacion entity.
     *
     * @param Asignacion $asignacion The asignacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Asignacion $asignacion) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('asignacion_delete', array('id' => $asignacion->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
