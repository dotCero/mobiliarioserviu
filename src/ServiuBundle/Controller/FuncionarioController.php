<?php

namespace ServiuBundle\Controller;

use ServiuBundle\Entity\Funcionario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Funcionario controller.
 *
 */
class FuncionarioController extends Controller {

    /**
     * Lists all funcionario entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $funcionarios = $em->getRepository('ServiuBundle:Funcionario')->findAll();

        return $this->render('funcionario/index.html.twig', array(
                    'funcionarios' => $funcionarios,
        ));
    }

    public function inventarioAction(Funcionario $funcionario) {
        $em = $this->getDoctrine()->getManager();
        $asignaciones = $em->getRepository('ServiuBundle:Asignacion')->findAll();

        return $this->render('funcionario/inventario.html.twig', ['asignaciones' => $asignaciones, 'f' => $funcionario]);
    }

    /**
     * Creates a new funcionario entity.
     *
     */
    public function newAction(Request $request) {
        $funcionario = new Funcionario();
        $form = $this->createForm('ServiuBundle\Form\FuncionarioType', $funcionario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($funcionario);
            $em->flush($funcionario);

            $this->addFlash("m", $funcionario->getNombre());

            return $this->redirectToRoute('funcionario_index', array('id' => $funcionario->getId()));
        }

        return $this->render('funcionario/new.html.twig', array(
                    'funcionario' => $funcionario,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a funcionario entity.
     *
     */
    public function showAction(Funcionario $funcionario) {
        $deleteForm = $this->createDeleteForm($funcionario);

        return $this->render('funcionario/show.html.twig', array(
                    'funcionario' => $funcionario,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing funcionario entity.
     *
     */
    public function editAction(Request $request, Funcionario $funcionario) {
        $deleteForm = $this->createDeleteForm($funcionario);
        $editForm = $this->createForm('ServiuBundle\Form\FuncionarioType', $funcionario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('funcionario_inventario', array('id' => $funcionario->getId()));
        }

        return $this->render('funcionario/edit.html.twig', array(
                    'funcionario' => $funcionario,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a funcionario entity.
     *
     */
    public function deleteAction(Request $request, Funcionario $funcionario) {
        $form = $this->createDeleteForm($funcionario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($funcionario);
            $em->flush($funcionario);
        }

        return $this->redirectToRoute('funcionario_index');
    }

    /**
     * Creates a form to delete a funcionario entity.
     *
     * @param Funcionario $funcionario The funcionario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Funcionario $funcionario) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('funcionario_delete', array('id' => $funcionario->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
