<?php

namespace ServiuBundle\Controller;

use ServiuBundle\Entity\Populate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Populate controller.
 *
 */
class PopulateController extends Controller
{


    /**
     * Creates a new populate entity.
     *
     */
    public function newAction(Request $request)
    {
        $populate = new Populate();
        $form = $this->createForm('ServiuBundle\Form\PopulateType', $populate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /*$em->persist($populate);
            $em->flush($populate);*/


            $file = $form['path']->getData();

            $dbhost = $this->getParameter('database_host');
            $dbuser = $this->getParameter('database_user');
            $dbpass = $this->getParameter('database_password');
            $connParams = $em->getConnection()->getParams();

            $pdoConn = new \PDO('mysql:host=' . $dbhost . ';dbname=' . $connParams['dbname'], $dbuser, $dbpass, array(
                \PDO::MYSQL_ATTR_LOCAL_INFILE => true
            ));

            $sql = "SET FOREIGN_KEY_CHECKS=0";
            $stmt = $pdoConn->prepare($sql);
            $stmt->execute();

            $sql = "LOAD DATA LOCAL INFILE '" . $file . "' INTO TABLE funcionario
                    CHARACTER SET UTF8
                    FIELDS TERMINATED BY \"|\"
                    LINES TERMINATED BY \"\n\"
                    (funcionario.rut, funcionario.nombre, funcionario.depto, funcionario.unidad, funcionario.cargo, funcionario.iniciales, funcionario.apellidoNombre)
                    set id = null;";

            $stmt = $pdoConn->prepare($sql);
            $result = $stmt->execute();

            if ($result) {
                $this->addFlash("m", $file->getClientOriginalName());
                return $this->redirectToRoute('populate_funcionario', array());
            } else {
                $this->addFlash("e", $file->getClientOriginalName());
                return $this->render('populate/importF.html.twig', array(
                    'populate' => $populate,
                    'form' => $form->createView(),
                ));
            }
        }

        return $this->render('populate/importF.html.twig', array(
            'populate' => $populate,
            'form' => $form->createView(),
        ));
    }
}
