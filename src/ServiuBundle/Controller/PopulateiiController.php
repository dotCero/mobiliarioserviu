<?php

namespace ServiuBundle\Controller;

use ServiuBundle\Entity\Populateii;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Populateii controller.
 *
 */
class PopulateiiController extends Controller
{

    /**
     * Creates a new populateii entity.
     *
     */
    public function newAction(Request $request)
    {
        $populateii = new Populateii();
        $form = $this->createForm('ServiuBundle\Form\PopulateiiType', $populateii);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /*$em->persist($populate);
            $em->flush($populate);*/


            $file = $form['path']->getData();

            $dbhost = $this->getParameter('database_host');
            $dbuser = $this->getParameter('database_user');
            $dbpass = $this->getParameter('database_password');
            $connParams = $em->getConnection()->getParams();

            $pdoConn = new \PDO('mysql:host=' . $dbhost . ';dbname=' . $connParams['dbname'], $dbuser, $dbpass, array(
                \PDO::MYSQL_ATTR_LOCAL_INFILE => true
            ));

            $sql = "SET FOREIGN_KEY_CHECKS=0";
            $stmt = $pdoConn->prepare($sql);
            $stmt->execute();

            $sql = "LOAD DATA LOCAL INFILE '" . $file . "' INTO TABLE mobiliario
                    CHARACTER SET UTF8
                    FIELDS TERMINATED BY \"|\"
                    LINES TERMINATED BY \"\n\"
                    (mobiliario.codigo, mobiliario.nombre, mobiliario.observacion, mobiliario.valor, mobiliario.fechaAdquisicion, mobiliario.vidaUtil, mobiliario.depreciacion, mobiliario.valorResidual, mobiliario.valorActualizado, mobiliario.grupo, mobiliario.categoria, mobiliario.subgrupo, mobiliario.medidas, mobiliario.caracteristicas)
                    set id = null;";

            $stmt = $pdoConn->prepare($sql);
            $result = $stmt->execute();

            if ($result) {
                $this->addFlash("m", $file->getClientOriginalName());
                return $this->redirectToRoute('populateii_mobiliario', array());
            } else {
                $this->addFlash("e", $file->getClientOriginalName());
                return $this->render('populateii/importM.html.twig', array(
                    'populateii' => $populateii,
                    'form' => $form->createView(),
                ));
            }
        }

        return $this->render('populateii/importM.html.twig', array(
            'populateii' => $populateii,
            'form' => $form->createView(),
        ));
    }
}
