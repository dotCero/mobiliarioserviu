<?php

namespace ServiuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of DumpController
 *
 * @author cero
 */
class DumpController extends Controller
{

    public function indexAction(Request $request)
    {
        $form = $this->createForm('ServiuBundle\Form\DumpType');
        $form->handleRequest($request);

        return $this->render('dump/index.html.twig', ['form' => $form->createView()]);
    }

    public function createAction(Request $request)
    {
        $this->crearDump();
        return $this->redirect($this->generateUrl('dump_index'));
    }

    public function restoreAction(Request $request)
    {
        $var = $request->get("serviubundle_dump");
        $dynamic = null;

        foreach ($var as $v) {
            $dynamic = $v;
            break;
        }

        $nameFile = explode('.sql', $dynamic);

        $fixIt = $nameFile[0] . '.sql';

        $this->restaurarDump($fixIt);
        return $this->redirect($this->generateUrl('dump_index'));


        //return $this->render('dump/restore.html.twig');
    }

    private function crearDump()
    {
        $dbname = $this->getParameter('database_name');
        $host = $this->getParameter("database_host");
        $user = $this->getParameter("database_user");
        $pass = $this->getParameter('database_password');

        $route = '/usr/moseDump';
        $separador = '/';

        if (strpos(PHP_OS, 'WIN')) {
            $route = 'C:\MoSeDump';
            $separador = '\\';
        }

        if (!file_exists($route . $separador)) {
            mkdir($separador . $route, 077);
        }

        $backup_file = $route . $separador . $dbname . "_" . date("dmYHis") . ".sql";
        $command = "mysqldump --opt -h " . $host . " -u " . $user . " -p" . $pass . " " . $dbname . " > $backup_file";

        //system($command, $output);
        exec($command);
    }

    private function restaurarDump($file_name)
    {
        $dbname = $this->getParameter('database_name');
        $host = $this->getParameter("database_host");
        $user = $this->getParameter("database_user");
        $pass = $this->getParameter('database_password');

        $route = '/usr/moseDump';
        $separador = '/';

        if (strpos(PHP_OS, 'WIN')) {
            $route = 'C:\MoSeDump';
            $separador = '\\';
        }

        $backup_file = $route . $separador . $file_name;
        $command = "mysql --user=" . $user . " --password=" . $pass . " " . $dbname . " < $backup_file";

        system($command, $output);

    }

    private function nothing()
    {
        $directorio = opendir("/home/cero/Documentos/pruebasDump"); //ruta actual
        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (is_dir($archivo)) {//verificamos si es o no un directorio
                //de ser un directorio lo envolvemos entre corchetes
            } else {
                $extension = explode(".", $archivo);
                if ($extension[count($extension) - 1] == "txt") {
                    echo $archivo;
                }
            }
        }
    }
}