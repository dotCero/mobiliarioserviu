<?php

namespace ServiuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReporteController extends Controller {

    public function reporteAction() {
        $em = $this->getDoctrine()->getManager();

        $query = 'select mobiliario.codigo, '
                . 'mobiliario.nombre, '
                . '(select sum(cantidadMobiliario) as cant from asignacion where idMobiliario=mobiliario.id) as cantidad, '
                . 'mobiliario.valor, '
                . 'mobiliario.fechaAdquisicion, '
                . 'mobiliario.vidaUtil, '
                . 'mobiliario.depreciacion, '
                . 'mobiliario.valorResidual, '
                . 'mobiliario.valorActualizado '
                . 'from asignacion inner join mobiliario on asignacion.idMobiliario = mobiliario.id '
                . 'group by asignacion.idMobiliario;'
        ;

        $dec = $em->getConnection()->prepare($query);

        $dec->execute();

        $reporte = $dec->fetchAll();
        
        return $this->render('reporte/reporte.html.twig', array(
                    'reporte' => $reporte,
        ));
    }

    public function reporte2Action() {
        $em = $this->getDoctrine()->getManager();

        $asignaciones = $em->getRepository('ServiuBundle:Asignacion')->findAll();

        return $this->render('reporte/reporte2.html.twig', ['asignaciones' => $asignaciones]);
    }

}
