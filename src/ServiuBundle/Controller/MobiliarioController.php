<?php

namespace ServiuBundle\Controller;

use ServiuBundle\Entity\Mobiliario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Mobiliario controller.
 *
 */
class MobiliarioController extends Controller {

    /**
     * Lists all mobiliario entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $mobiliarios = $em->getRepository('ServiuBundle:Mobiliario')->findAll();

        return $this->render('mobiliario/index.html.twig', array(
                    'mobiliarios' => $mobiliarios,
        ));
    }

    /**
     * Creates a new mobiliario entity.
     *
     */
    public function newAction(Request $request) {
        $mobiliario = new Mobiliario();
        $form = $this->createForm('ServiuBundle\Form\MobiliarioType', $mobiliario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $repo = $this->getDoctrine()->getRepository('ServiuBundle:Mobiliario');

            $mobi = $repo->findOneByCodigo($mobiliario->getCodigo());

            if ($mobi) {
                $this->addFlash("m", $mobi->getCodigo());
                
                return $this->render('mobiliario/new.html.twig', array(
                            'mobiliario' => $mobiliario,
                            'form' => $form->createView(),
                ));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($mobiliario);
            $em->flush($mobiliario);

            $this->addFlash("m", $mobiliario->getNombre());

            return $this->redirectToRoute('mobiliario_show', array('id' => $mobiliario->getId()));
        }

        return $this->render('mobiliario/new.html.twig', array(
                    'mobiliario' => $mobiliario,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a mobiliario entity.
     *
     */
    public function showAction(Mobiliario $mobiliario) {
        $deleteForm = $this->createDeleteForm($mobiliario);

        return $this->render('mobiliario/show.html.twig', array(
                    'mobiliario' => $mobiliario,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing mobiliario entity.
     *
     */
    public function editAction(Request $request, Mobiliario $mobiliario) {
        $deleteForm = $this->createDeleteForm($mobiliario);
        $editForm = $this->createForm('ServiuBundle\Form\MobiliarioType', $mobiliario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mobiliario_show', array('id' => $mobiliario->getId()));
        }

        return $this->render('mobiliario/edit.html.twig', array(
                    'mobiliario' => $mobiliario,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a mobiliario entity.
     *
     */
    public function deleteAction(Request $request, Mobiliario $mobiliario) {
        $form = $this->createDeleteForm($mobiliario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($mobiliario);
            $em->flush($mobiliario);
        }

        return $this->redirectToRoute('mobiliario_index');
    }

    /**
     * Creates a form to delete a mobiliario entity.
     *
     * @param Mobiliario $mobiliario The mobiliario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Mobiliario $mobiliario) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('mobiliario_delete', array('id' => $mobiliario->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function reporteAction() {
        $em = $this->getDoctrine()->getManager();

        $mobiliarios = $em->getRepository('ServiuBundle:Mobiliario')->findAll();

        return $this->render('mobiliario/reporte.html.twig', array('mobiliarios' => $mobiliarios));
    }

}
