<?php

namespace ServiuBundle\Controller;

use ServiuBundle\Form\DumpType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        // esta es la ruta del index...
//        $translated = $this->get('translator')->trans('Back to the list');
//
//        return new Response($translated);
        //echo PHP_OS;

        $em = $this->getDoctrine()->getManager();

        $funcionarios = $em->getRepository('ServiuBundle:Funcionario')->findAll();
        $mobiliario = $em->getRepository('ServiuBundle:Mobiliario')->findAll();
        $asignaciones = $em->getRepository('ServiuBundle:Asignacion')->findAll();

        $cont = 0;

        foreach ($asignaciones as $a){
            $cont = $cont + $a->getCantidadmobiliario();
        }

        $dt = new DumpType();

        return $this->render('default/indexTwo.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'fSize' => count($funcionarios),
            'mSize' => count($mobiliario),
            'aSize' => count($asignaciones),
            'rSize' => $cont,
            'dSize' => count($dt->getList()),
        ]);
    }

    public function indexTwoAction()
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }

    private function nothing()
    {
        $dbname = $this->getParameter('database_name');
        $host = $this->getParameter("database_host");
        $user = $this->getParameter("database_user");
        $pass = $this->getParameter('database_password');

        $backup_file = "/home/cero/Documentos/pruebasDump/" . $dbname . "_" . date("dmYHis") . ".sql";
        $command = "mysqldump --opt -h " . $host . " -u " . $user . " -p" . $pass . " " . $dbname . " > $backup_file";

        system($command, $output);
    }

}

